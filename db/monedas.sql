create table XRP_30mins(
    id serial,
    Open TEXT NOT NULL,
    Close TEXT NOT NULL,
    High TEXT NOT NULL,
    Low TEXT NOT NULL,
    Time TEXT NOT NULL,
    Volume TEXT NOT NULL,
    BVolume TEXT NOT NULL
);

