# comandos
# deps 
#  docker
#  docker-compose
#  dockerfile

[img](https://www.plantuml.com/plantuml/img/SoWkIImgAStDuGA2lFoKL8Na_BoInCoyr293XMek1GM5w9IaMAvQT4aZi6HqOa5cV45bIKP-NYmNM2cS1zJ1XIojOADh1rOVYoeC4WNPWAGNg1GYnkMGcfS2T1i0)

# docker install
curl -L get.docker.com | sh

# docker-compose
# instala docker-compose
sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

# levantar containers del compose.yml
docker-compose  up -d  # debes estar en el mismo directorio del docker-compose.yml

# lista containers online
docker ps  

# ingresar al container
docker exec -it NOMBRE_CONTAINER bash | sh


# comando para ver tabla db
echo "select * from monedas;" |docker exec -i portfolio_db psql -U portfolio -w portfolio  -dportfolio


# git

# iniciar directorio como repo de git
git init 
git remote URL REPO

# guardar cambios
git add .

# commitear cambios y poner mensaje de cambios
git commit -m "mensaje"

# empujar cambios al github/gitlab
git push

